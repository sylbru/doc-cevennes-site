const path = require('path');
const gulp = require('gulp');
const plugins = require('gulp-load-plugins')();
const log = require('fancy-log');
const server = require('browser-sync').create();

const paths = {
    scss: {
        main: "src/scss/main.scss",
        files: "src/scss/**/*.scss",
        standalone: [
          "src/scss/sections/festival.accueil.scss",
          "src/scss/sections/homepage.reseau.scss",
          "src/scss/admin/seances-festival.scss",
        ],
    },
};

const DEBOUNCE = 500;
const handleError = plugins.notify.onError("<%= error.message %>")

function buildCss(done) {
   log("Building CSS.");

   let files = paths.scss.standalone;
   files.push(paths.scss.main);

   gulp.src(files)
      .pipe(plugins.plumber({
         errorHandler: handleError
      }))
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.sass({
        outputStyle: "compressed",
      }))
      .pipe(plugins.sourcemaps.write('sourcemaps'))
      .pipe(plugins.stripbom())
      .pipe(gulp.dest('dist/css'))
      .on('end', done);

}

const watchCss = function () {
    gulp.watch(paths.scss.files, gulp.series(buildCss, reload));
};

function reload(done) {
  server.reload();
  done();
}

function serve(done) {
  server.init({
    proxy: "doc-cevennes.local",
    port: 2002,
  });
  done();
}

gulp.task('serve', serve);

gulp.task('dev', gulp.series(serve, watchCss));
gulp.task('default', watchCss);

gulp.task('build:css', buildCss);
gulp.task('build', gulp.parallel('build:css'));
