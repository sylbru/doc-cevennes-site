<?php

if (!empty($_GET["file"])) {
	$parsed = parse_url($_GET["file"]);

	if ($parsed === false) {
		http_response_code(400);
		die("Adresse invalide.");
	}

	if (isset($parsed["scheme"]) || isset($parsed["host"])) {
		http_response_code(403);
		die("L'adresse doit être locale.");
	}

	$hit_count = @file_get_contents($_GET["file"]."_counter");
	$hit_count = intval($hit_count) + 1;
	@file_put_contents($_GET["file"]."_counter", $hit_count);

	header('Location: '.$_GET["file"]);
} else {
	http_response_code(400);
	die("Fichier non renseigné.");
}