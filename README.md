# Site web DOC-Cévennes 2018

## Développement

* Sur localhost Apache ou avec le serveur intégré PHP par exemple (`cd public/ && php7.2 -S 0.0.0.0:1111`).
* Code source hébergé sur GitLab
* Ne fonctionne pas avec PHP 7.4 ou supérieur ! Utiliser PHP 7.2.
* Extensions requises : sqlite, xml et autres (https://docs.boltcms.io/3.7/getting-started/requirements)

### Technologies

* Backend : Bolt CMS 3.4.10
* Frontend :
  * SASS, architecture type ITCSS
  * Gulp (lancer simplement `gulp` dans le dossier `/public/theme/doc-cevennes-2018`, tâche par défaut watchCss)

### Fonte page d’accueil

On utilise la police Aaargh en CSS inline (`/public/theme/doc-cevennes-2018/fonts/aaargh.css`). La taille du fichier est réduite au minimum à l’aide du [générateur de webfonts de FontSquirrel](https://www.fontsquirrel.com/tools/webfont-generator). Si jamais il manque des caractères, il faut le générer à nouveau, en important la fonte d’origine `fonts/Aaargh.ttf` et la configuration `fonts/generator_config.txt` (en ajoutant les caractères manquants avant de générer le kit).

## Déploiement

* Avec [dg/ftp-deployment](https://github.com/dg/ftp-deployment)
* `php deployment.phar deployment.ovh.ini`
  * ajouter `-t` pour faire une simulation
  * `ovh` est le serveur actuel, `prod` et `test` sont sur l’ancien hébergement lautre.net, `vps` était pour un test sur un VPS perso
* Il y a un système de cache assez agressif, il faut donc en général se connecter sur l’administration et vidanger le cache pour voir les changements. http://doc-cevennes.org/admin/clearcache

## Mise à jour annuelle pour la nouvelle édition

* Archiver d’abord l’édition passée (voir plus bas) ! Il est important de faire l’archivage
  en premier, car sinon la version archivée de l’année N contiendra les dates de l’année
  N+1, et surtout la programmation sera cachée…
* Changer les dates et le numéro de l’édition dans `/public/theme/doc-cevennes-2018/accueil_annonce_festival.twig` et `/public/theme/doc-cevennes-2018/partials/festival_annonce.twig`
* Changer la date de fin des inscriptions dans `/public/theme/doc-cevennes-2018/sections/festival/formulaire-inscription.twig`
  * Mettre un rappel pour désactiver les inscriptions à cette date ? Au moins envoyer un mail à ce moment-là
* Changer les dates possibles pour les séances dans `/app/config/contenttypes.yml` (`fest_seances.fields.horaire.options.datepicker`)
* Sur le site, modifier `/app/config/config_local.yml` pour mettre `inscriptions_ouvertes` et `cacher_programmation_festival` à true

### Archivage

Chaque année on archive le contenu du site avec HTTrack, avant de préparer le site pour la nouvelle édition.

```
cd public/archives
httrack https://doc-cevennes.org/ -O httrack_tmp
mv httrack_tmp/doc-cevennes.org/ 20XX
rm -rf httrack_tmp/ 20XX/archives/ 20XX/festival-de-lasalle/archives*
```

Finir en enlevant le `href` des liens « Archives » dans le nouveau dossier (sauf section Réseau), et en leur ajoutant la classe `not-yet` (avec un éditeur de texte par exemple, pas beaucoup d’occurrences).

Concrètement : rechercher les `archives.html` dans `public/archives/2022/*.*,-public/archives/2022/reseau/,-public/archives/2022/reseau.html`.

## Bugs connus

* Sur la page d’accueil, la balise `<link rel="canonical"…` indique `http://doc-cevennes.local/accueil/slug-4838d0951a4818adb25ebd1b99995b94` au lieu de `http://doc-cevennes.local/`. Apparemment c’est lié au fait que la route `contentlink` est utilisée au lieu de `homepage` (voir barre d’outils Symfony). Mais pourquoi ?
