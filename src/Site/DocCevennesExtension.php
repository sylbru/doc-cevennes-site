<?php

namespace Bundle\Site;

use Bolt\Extension\AbstractExtension;
use Bolt\Extension\SimpleExtension;
use Bolt\Extension\TwigTrait;
use Bolt\Menu\MenuEntry;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Site bundle extension loader.
 *
 * For more information on building bundles see https://docs.bolt.cm/extensions
 */
class DocCevennesExtension extends SimpleExtension implements ServiceProviderInterface
{
    const MOIS_FR = [
        "january" => "janvier",
        "february" => "février",
        "march" => "mars",
        "april" => "avril",
        "may" => "mai",
        "june" => "juin",
        "july" => "juillet",
        "august" => "août",
        "september" => "septembre",
        "october" => "octobre",
        "november" => "novembre",
        "december" => "décembre",
    ];

    const JOURS_FR = [
        "monday" => "lundi",
        "tuesday" => "mardi",
        "wednesday" => "mercredi",
        "thursday" => "jeudi",
        "friday" => "vendredi",
        "saturday" => "samedi",
        "sunday" => "dimanche",
    ];

    public function registerFrontendControllers()
    {
        return [
            "/festival-de-lasalle/programmation" => new Controller\ProgrammationFestivalController(
                "/festival-de-lasalle/programmation"
            ),
            "/reseau" => new Controller\ProgrammationReseauController(
                "/reseau"
            ),
        ];
    }

    public function registerFrontendRoutes(ControllerCollection $collection)
    {
        $collection->get("/festival-de-lasalle/archives", [$this, "archivesFestival"])
            ->bind("festival_archives_accueil");
        $collection->get("/festival-de-lasalle/archives/{annee}", [$this, "archivesFestival"])
            ->bind("festival_archives_annee");
    }

    protected function registerTwigFilters()
    {
        return [
            "fr_datetime" => "datetimeFilter",
            "fr_date" => "dateFilter",
            "fr_datemonth" => "dateMonthFilter",
            "fr_time" => "timeFilter",
            "uniq" => "uniqFilter",
            "base64" => "base64Filter",
        ];
    }

    private function commonDateTimeFilter($date, $format)
    {
        $date_ = new \DateTime($date);

        // setlocale(LC_ALL, "fr_FR"); // Inutile car pas activé sur notre hébergeur
        $dateEN = strftime($format, $date_->getTimestamp());
        $dateFR = str_ireplace(array_keys(self::MOIS_FR), array_values(self::MOIS_FR), $dateEN);
        $dateFR = str_ireplace(array_keys(self::JOURS_FR), array_values(self::JOURS_FR), $dateFR);

        $dateFR = preg_replace("/ 1 /", ' 1er ', $dateFR);

        return $dateFR;
    }

    public function datetimeFilter($date)
    {
        return $this->commonDateTimeFilter($date, "%A %e %B à %kh%M");
    }

    public function dateFilter($date)
    {
        return $this->commonDateTimeFilter($date, "%A %e %B");
    }

    public function dateMonthFilter($date)
    {
        return $this->commonDateTimeFilter($date, "%B %Y");
    }

    public function timeFilter($date)
    {
        return $this->commonDateTimeFilter($date, "%kh%M");
    }

    public function uniqFilter(array $array)
    {
        return array_unique($array);
    }

    public function base64Filter($data)
    {
        return base64_encode($data);
    }

    protected function registerBackendRoutes(ControllerCollection $collection)
    {
        $collection->get("/seances-festival", [$this, "seancesFestival"])
            ->bind("admin_seances_festival");
        $collection->get("/seances-reseau", [$this, "seancesReseau"])
            ->bind("admin_seances_reseau");
    }

    /**
     * {@inheritdoc}
     */
    protected function registerMenuEntries()
    {
        return [
            (new MenuEntry('seances-festival', 'seances-festival'))
                ->setLabel("Séances Festival")
                ->setIcon("fa:table")
                ->setRoute("admin_seances_festival")
                ->setPermission("seances_festival"),
            (new MenuEntry('seances-reseau', 'seances-reseau'))
                ->setLabel("Séances Réseau")
                ->setIcon("fa:calendar")
                ->setRoute("admin_seances_reseau")
                ->setPermission("seances_reseau"),
        ];
    }

    public function seancesFestival(Application $app, Request $req)
    {
        $seancesRepo = $app["storage"]->getRepository("fest_seances");
        $filmsRepo = $app["storage"]->getRepository("fest_films");
        $lieuxRepo = $app["storage"]->getRepository("fest_lieux");

        $seances = $seancesRepo->findAll();

        $lieux = $lieuxRepo->findAll();
        $lieuxParId = [];
        foreach ($lieux as $lieu) {
            $lieuxParId[$lieu->getId()] = $lieu;
        }

        $films = $filmsRepo->findAll();
        $filmsParId = [];

        if (empty($films)) {
            $films = [];
        }

        foreach ($films as $film) {
            $filmsParId[$film->getId()] = $film;
        }

        $jours = [];
        $grilleHoraire = [];

        if (empty($seances)) {
            return $this->renderTemplate("admin/seances_festival.twig", [
                "grille" => null,
                "jours" => $jours,
                "lieux" => $lieuxParId,
                "films" => $filmsParId,
            ]);
        }

        $sansLieuOuHoraire = [];

        foreach ($seances as $seance) {
            $seancesById[$seance->getId()] = $seance;

            if ($seance->getHoraire()) {
                $horaire = new \DateTime($seance->getHoraire());
                $jour = $horaire->format("Y-m-d");
            } else {
                $sansLieuOuHoraire[] = $seance;
                continue;
            }

            if ($seance->getLieu()) {
                $lieu = intval($seance->getLieu());
            } else {
                $sansLieuOuHoraire[] = $seance;
                continue;
            }

            if (!array_key_exists($lieu, $grilleHoraire)) {
                $grilleHoraire[$lieu] = [];
            }

            if (!array_key_exists($jour, $grilleHoraire[$lieu])) {
                $grilleHoraire[$lieu][$jour] = [];

                if (!in_array($jour, $jours)) {
                    $jours[] = $jour;
                }
            }

            $grilleHoraire[$lieu][$jour][] = $seance;
        }

        foreach ($grilleHoraire as $lieu_ => $jours_) {
            foreach ($jours_ as $jour_ => $seances_) {
                usort($grilleHoraire[$lieu_][$jour_], function ($a, $b)
                {
                    return ($a->getHoraire() < $b->getHoraire()) ? -1 : 1;
                });
            }
        }

        return $this->renderTemplate("admin/seances_festival.twig", [
            "sans_lieu_ou_horaire" => $sansLieuOuHoraire,
            "grille" => $grilleHoraire,
            "jours" => $jours,
            "lieux" => $lieuxParId,
            "films" => $filmsParId,
        ]);
    }

    public function seancesReseau(Application $app, Request $req)
    {
        $seancesRepo = $app["storage"]->getRepository("reso_seances");
        $filmsRepo = $app["storage"]->getRepository("reso_films");
        $lieuxRepo = $app["storage"]->getRepository("reso_lieux");

        $seances = $seancesRepo->findAll();

        $lieux = $lieuxRepo->findAll();
        $lieuxParId = [];
        foreach ($lieux as $lieu) {
            $lieuxParId[$lieu->getId()] = $lieu;
        }

        $films = $filmsRepo->findAll();
        $filmsParId = [];

        if (empty($films)) {
            $films = [];
        }

        foreach ($films as $film) {
            $filmsParId[$film->getId()] = $film;
        }

        return $this->renderTemplate("admin/seances_reseau.twig", [
            "seances" => $seances,
            "lieux" => $lieuxParId,
            "films" => $filmsParId,
        ]);
    }

    public function archivesFestival(Application $app, Request $req, $annee = null)
    {
        $path = "./archives/";
        if (!file_exists($path)) {
            throw new \Exception("Path does not exist.");
        }

        if (!is_dir($path)) {
            throw new \Exception("Path is not a directory.");
        }

        $dirs = scandir($path);
        $archives = array_values(array_filter($dirs,
            function ($dir) use ($path) {
                return is_dir($path . $dir)
                    && substr($dir, 0, 1) !== ".";
            }
        ));

        $indexes = [];
        $enterPages = [
            "edito.html", "edito.htm",
            "accueil.html", "accueil.htm",
            "index.html", "index.html",
            "index.htm", "index.htm",
        ];

        foreach ($archives as $dir) {
            for ($i = 0; $i < count($enterPages); $i++) {
                $page = $enterPages[$i];
                if (file_exists($path . $dir . "/" . $page)) {
                    $indexes[$dir] = $page;
                    break;
                }
            }
        }

        return $this->renderTemplate("archives.twig", [
            "annee" => $annee,
            "archives" => $indexes,
        ]);
    }

    public function registerServices(Application $app)
    {
        $app['doc-cevennes'] = $app->share(function () { return $this; });
    }

    public function redirectOldPage(Application $app, Request $req)
    {
        $mapping = [
            "index" => "/",
            "edito" => "/festival-de-lasalle",
            "films" => "/festival-de-lasalle/programmation/films",
            "intervenants" => "/festival-de-lasalle/programmation/intervenants",
            "infos" => "/festival-de-lasalle/infos-pratiques",
            "partenaires" => "/festival-de-lasalle/partenaires",
            "liens" => "/festival-de-lasalle/partenaires", // Ou page Lien sur /association ?
            "archives" => "/festival-de-lasalle/archives",
            "telechargement" => "/festival-de-lasalle/telechargements",
            "association" => "/association-champ-contrechamp",
            "anduze" => "/reseau",
            "ganges" => "/reseau",
            "levigan" => "/reseau",
            "pont" => "/reseau",
            "ispagnac" => "/reseau",
            "vialas" => "/reseau",
            "valleraugue" => "/reseau",
            "florac" => "/reseau",
        ];
        $page = $req->get("slug");

        if (!empty($mapping[$page])) {
            return new RedirectResponse($mapping[$page]);
        } else {
            return new Response($this->renderTemplate("notfound.twig", []), Response::HTTP_NOT_FOUND);
        }
    }

    public function tmpFixFB(Application $app, Request $req)
    {
        $uri = $req->getRequestUri();

        if ($uri === "/festival-de") {
            return new RedirectResponse("/festival-de-lasalle/programmation/lieux");
        } elseif ($uri === "/festival-de-las") {
            return new RedirectResponse("/festival-de-lasalle/infos-pratiques");
        }
    }

    public function formulaireInscription(Application $app, Request $req)
    {
        $recaptchaSiteKey = $app['config']->get("general/recaptcha_site_key");
        $recaptchaSecretKey = $app['config']->get("general/recaptcha_secret_key");
        if (empty($recaptchaSiteKey) || empty($recaptchaSecretKey)) {
            throw new \Exception("Configuration reCAPTCHA invalide.");
        }

        $data = [
            "current_year" => date("Y"),
            "recaptcha_site_key" => $recaptchaSiteKey,
        ];

        if ($req->getMethod() === "POST") {
            $post = $req->request->all();
            $data["postdata"] = $post;

            $mailHtml = $this->renderTemplate("sections/festival/email-inscription.twig",
                ["postdata" => $post]
            );

            if (!empty($post["titre-francais"]) && !empty($post["titre-original"])) {
                $subject = $post["titre-francais"] . " (" . $post["titre-original"] . ")";
            } else if (!empty($post["titre-francais"])) {
                $subject = $post["titre-francais"];
            } else if (!empty($post["titre-original"])) {
                $subject = $post["titre-original"];
            } else {
                $subject = "...";
            }
            $subject = "[Inscription] " . $subject;

            $boiteInscription = $app['config']->get("general/boite_inscription");
            $errorDetails = "";

            if (!empty($boiteInscription)) {
                $mailer = $app['mailer'];
                $message = $mailer
                    ->createMessage('message')
                    ->setSubject($subject)
                    ->setFrom(["postmaster@doc-cevennes.com" => $app['config']->get('general/sitename')])
                    ->setTo([
                        $boiteInscription => "Sélection Champ-Contrechamp",
                        $post["real-email"] => mb_strtoupper($post["real-nom"]) . " "
                            . mb_convert_case($post["real-prenom"], MB_CASE_TITLE),
                    ])
                    ->setBody($mailHtml)
                    ->addPart($mailHtml, 'text/html');

                $success = false;
                $failedRecipients = [];
                try {
                    $this->checkRecaptcha($post, $recaptchaSecretKey);

                    $recipients = $mailer->send($message, $failedRecipients);

                    // Try and send immediately
                    $app['swiftmailer.spooltransport']
                        ->getSpool()->flushQueue($app['swiftmailer.transport']);

                    if ($recipients) {
                        $success = true;
                    }
                } catch (\Throwable $e) {
                    $errorDetails = $e->getMessage();
                    // Notify below
                }
            } else {
                $success = false;
                $errorDetails = "L’adresse d’envoi n’est pas définie.";
            }

            if ($success) {
                $data["feedback"] = [
                    "class" => "success",
                    "message" => "Merci pour votre inscription !"
                        . (empty($post["visionnage-lien"])
                            ? " N’oubliez pas de nous envoyer un support de visionnage."
                            : "")
                ];
            } else {
                $data["mail_body"] = $mailHtml;
                $data["mail_subject"] = $subject;

                $data["feedback"] = [
                    "class" => "fail",
                    "message" => "L’envoi a échoué, merci de nous envoyer votre inscription (résumé disponible ci-dessous)
                        par e-mail"
                        . (!empty($boiteInscription)
                            ? " à l’adresse <a href='mailto:" . $boiteInscription . "'>"
                                . $boiteInscription . "</a>"
                            : "")
                        . ". Merci de nous excuser de ce désagrément.",
                    "details" => "Détails : " . $errorDetails,
                ];
            }
        }

        return $this->renderTemplate("sections/festival/formulaire-inscription.twig", $data);
    }

    /** @throws CaptchaFailed */
    private function checkRecaptcha($post, $secretKey): void
    {
        if (empty($post["g-recaptcha-response"])) {
            throw new CaptchaFailed("Pas de champ g-recaptcha-response dans $_POST");
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(["secret" => $secretKey, "response" => $post["g-recaptcha-response"]]));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);
        curl_setopt($ch, CURLINFO_HEADER_OUT, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);

        $response = curl_exec($ch);
        curl_close($ch);

        $parsedResponse = json_decode($response);
        $success = $parsedResponse->success ?? false;

        if (!$success) {
            throw new CaptchaFailed($response);
        }
    }
}
