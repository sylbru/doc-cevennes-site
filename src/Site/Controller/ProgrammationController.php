<?php

namespace Bundle\Site\Controller;

use Bolt\Controller\Base;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

abstract class ProgrammationController extends Base
{
    protected $routePrefix;
    protected $contenttypeFilms;
    protected $contenttypeSeances;
    protected $contenttypeLieux;
    protected $contenttypeIntervenants;
    protected $templateFilms;
    protected $templateSeancesSpeciales;
    protected $templateIntervenants;
    protected $templateSeances;
    protected $templateArchives;
    protected $mountPoint;

    const SEANCES_FUTURES = 1;
    const SEANCES_PASSEES = -1;

    public function __construct($mountPoint = "")
    {
        $this->mountPoint = $mountPoint;
    }

    public function render($template, array $context = [], array $globals = [])
    {
        $context["mount_point"] = $this->mountPoint;

        return parent::render($template, $context, $globals);
    }

    public function addRoutes(ControllerCollection $c)
    {
        $c->match('/', [$this, "home"])
            ->bind($this->routePrefix."_programmation");
        $c->match('/films', [$this, "films"])
            ->bind($this->routePrefix."_programmation_films");
        $c->match('/seances-speciales', [$this, "seancesSpeciales"])
            ->bind($this->routePrefix."_programmation_seances_speciales");
        $c->match('/intervenants', [$this, "intervenants"])
            ->bind($this->routePrefix."_programmation_intervenants");
        $c->match('/seances', [$this, "seancesFutures"])
            ->bind($this->routePrefix."_programmation_seances_futures");
        $c->match('/grille-horaire', [$this, "seancesToutes"])
            ->bind($this->routePrefix."_programmation_seances_toutes");
        $c->match('/archives', [$this, "seancesPassees"])
            ->bind($this->routePrefix."_programmation_seances_passees");
        $c->match('/lieux', [$this, "lieux"])
            ->bind($this->routePrefix."_programmation_lieux");
        $c->match('/lieux/{slug}', [$this, "lieu"])
            ->bind($this->routePrefix."_programmation_lieu");

        return $c;
    }

    public function home(Request $request)
    {
        return $this->render($this->templateAccueil);
    }

    public function films(Request $request, Application $app)
    {
        $seancesRepo = $app["storage"]->getRepository($this->contenttypeSeances);
        $filmsRepo = $app["storage"]->getRepository($this->contenttypeFilms);
        $intervenantsRepo = $app["storage"]->getRepository($this->contenttypeIntervenants);

        $seances = $seancesRepo->findAll();
        $seancesParFilm = [];

        $films = $filmsRepo->findBy([], ["titre", "ASC"]);

        $intervenantsParId = [];
        $intervenants = $intervenantsRepo->findAll();

        if (empty($intervenants)) {
            $intervenantsParId = [];
        } else {
            foreach ($intervenants as $intervenant) {
                $intervenantsParId[$intervenant->getId()] = $intervenant;
            }
        }

        if (empty($seances)) {
            $seancesParFilm = [];
            $lieuxById = [];
        } else {
            foreach ($seances as $seance) {
                if (!empty($seance->getFilms())) {
                    foreach($seance->getFilms() as $film) {
                        if (empty($seancesParFilm[$film])) {
                            $seancesParFilm[$film] = [];
                        }

                        array_push($seancesParFilm[$film], $seance);
                    }
                }
            }

            $lieuxRepo = $app["storage"]->getRepository($this->contenttypeLieux);
            $lieuxById = [];

            $lieux = $lieuxRepo->findAll();
            foreach ($lieux as $lieu) {
                $lieuxById[$lieu->getId()] = $lieu;
            }
        }

        return $this->render($this->templateFilms, [
            "intervenants" => $intervenantsParId,
            "seances" => $seancesParFilm,
            "films" => $films,
            "lieux" => $lieuxById,
        ]);
    }

    public function seancesSpeciales(Application $app)
    {
        $seancesRepo = $app["storage"]->getRepository($this->contenttypeSeances);

        $qb = $seancesRepo->createQueryBuilder("s")
            ->where('speciale_intitule != ""');
        $seances = $seancesRepo->findWith($qb);

        $intervenantsRepo = $app["storage"]->getRepository($this->contenttypeIntervenants);
        $intervenantsParId = [];
        $intervenants = $intervenantsRepo->findAll();
        foreach ($intervenants as $intervenant) {
            $intervenantsParId[$intervenant->getId()] = $intervenant;
        }

        $lieuxRepo = $app["storage"]->getRepository($this->contenttypeLieux);
        $lieuxById = [];
        $lieux = $lieuxRepo->findAll();
        foreach ($lieux as $lieu) {
            $lieuxById[$lieu->getId()] = $lieu;
        }

        // Récupération des films classés par id
        $filmsRepo = $app["storage"]->getRepository($this->contenttypeFilms);
        $filmsById = [];
        $films = $filmsRepo->findAll();
        if (!empty($films)) {
            foreach ($films as $film) {
                $filmsById[$film->getId()] = $film;
            }
        }

        return $this->render($this->templateSeancesSpeciales, [
            "seances" => $seances,
            "intervenants" => $intervenantsParId,
            "films" => $filmsById,
            "lieux" => $lieuxById,
        ]);
    }

    public function intervenants(Application $app)
    {
        $seancesRepo = $app["storage"]->getRepository($this->contenttypeSeances);
        $filmsRepo = $app["storage"]->getRepository($this->contenttypeFilms);
        $intervenantsRepo = $app["storage"]->getRepository($this->contenttypeIntervenants);

        $intervenants = $intervenantsRepo->findBy([], ["nom", "ASC"]);

        // Récupération des lieux classés par id
        $lieuxRepo = $app["storage"]->getRepository($this->contenttypeLieux);
        $lieuxParId = [];
        $lieux = $lieuxRepo->findAll();
        foreach ($lieux as $lieu) {
            $lieuxParId[$lieu->getId()] = $lieu;
        }

        // Récupération des films classés par id
        $filmsRepo = $app["storage"]->getRepository($this->contenttypeFilms);
        $filmsParId = [];
        $films = $filmsRepo->findAll();
        if (!empty($films)) {
            foreach ($films as $film) {
                $filmsParId[$film->getId()] = $film;
            }
        }

        $seances = $seancesRepo->findAll();
        $seancesParIntervenant = [];
        $rolesParIntervenant = [];

        if (!is_array($seances)) {
            $seances = [];
        }

        foreach ($seances as $seance) {
            if (!empty($seance->getIntervenants())) {
                foreach ($seance->getIntervenants() as $intervenant) {
                    $id = $intervenant->getRenderedValue("intervenant");
                    $role = $intervenant->getRenderedValue("role");

                    if (!array_key_exists($id, $seancesParIntervenant)) {
                        $seancesParIntervenant[$id] = [];
                    }

                    $seancesParIntervenant[$id][] = $seance;

                    if (!empty($role)) {
                        if (!array_key_exists($id, $rolesParIntervenant)) {
                            $rolesParIntervenant[$id] = [];
                        }

                        $rolesParIntervenant[$id][] = $role;
                    }

                }
            }
        }

        return $this->render($this->templateIntervenants, [
            "intervenants" => $intervenants,
            "seances" => $seancesParIntervenant,
            "roles" => $rolesParIntervenant,
            "lieux" => $lieuxParId,
            "films" => $filmsParId,
        ]);
    }

    public function seancesToutes(Application $app)
    {
        return $this->seances($app);
    }

    public function seancesFutures(Application $app)
    {
        return $this->seances($app, self::SEANCES_FUTURES);
    }

    public function seancesPassees(Application $app)
    {
        return $this->seances($app, self::SEANCES_PASSEES, $this->templateArchives);
    }

    public function seances(Application $app, $filter = null, $overrideTemplate = null)
    {
        $seancesRepo = $app["storage"]->getRepository($this->contenttypeSeances);
        $filmsRepo = $app["storage"]->getRepository($this->contenttypeFilms);
        $lieuxRepo = $app["storage"]->getRepository($this->contenttypeLieux);

        if ($filter === self::SEANCES_PASSEES || $filter === self::SEANCES_FUTURES) {
            $qb = $seancesRepo->createQueryBuilder("s")
                ->andWhere("s.horaire"
                    . ($filter === self::SEANCES_FUTURES ? " > " : " < ") . ":date"
                    )->setParameter("date", (new \DateTime())->format('Y-m-d H:i:s'))
                ->addOrderBy("horaire", "ASC");
            $seances = $seancesRepo->findWith($qb);
        } else {
            $qb = $seancesRepo->createQueryBuilder("s")
                ->andWhere("s.horaire IS NOT NULL AND s.horaire != ''")
                ->addOrderBy("horaire", "ASC");

            $seances = $seancesRepo->findWith($qb);
        }

        $lieux = $lieuxRepo->findAll();
        $lieuxParId = [];
        foreach ($lieux as $lieu) {
            $lieuxParId[$lieu->getId()] = $lieu;
        }

        $films = $filmsRepo->findAll();
        $filmsParId = [];
        if (!empty($films)) {
            foreach ($films as $film) {
                $filmsParId[$film->getId()] = $film;
            }
        }

        $jours = [];
        $grilleHoraire = [];

        foreach ($seances as $seance) {
            $seancesById[$seance->getId()] = $seance;

            $horaire = new \DateTime($seance->getHoraire());
            $jour = $horaire->format("Y-m-d");
            $lieu = intval($seance->getLieu());

            if (!array_key_exists($lieu, $grilleHoraire)) {
                $grilleHoraire[$lieu] = [];
            }

            if (!array_key_exists($jour, $grilleHoraire[$lieu])) {
                $grilleHoraire[$lieu][$jour] = [];

                if (!in_array($jour, $jours)) {
                    $jours[] = $jour;
                }
            }

            $grilleHoraire[$lieu][$jour][] = $seance;
        }

        foreach ($grilleHoraire as $lieu_ => $jours_) {
            foreach ($jours_ as $jour_ => $seances_) {
                usort($grilleHoraire[$lieu_][$jour_], function ($a, $b)
                {
                    return ($a->getHoraire() < $b->getHoraire()) ? -1 : 1;
                });
            }
        }

        return $this->render((!empty($overrideTemplate)) ? $overrideTemplate : $this->templateSeances, [
            "grille" => $grilleHoraire,
            "seances" => $seances,
            "jours" => $jours,
            "lieux" => $lieuxParId,
            "films" => $filmsParId,
        ]);
    }

    public function lieux(Application $app, Request $req)
    {
        $lieuxRepo = $app["storage"]->getRepository($this->contenttypeLieux);

        $lieux = $lieuxRepo->findAll();

        return $this->render($this->templateLieux, [
            "lieux" => $lieux
        ]);
    }

    public function lieu(Application $app, Request $req, $slug = null)
    {
        $seancesRepo = $app["storage"]->getRepository($this->contenttypeSeances);
        $lieuxRepo = $app["storage"]->getRepository($this->contenttypeLieux);
        $filmsRepo = $app["storage"]->getRepository($this->contenttypeFilms);

        $lieu = $lieuxRepo->findOneBy(["slug" => $slug]);
        $seances = $seancesRepo->findBy(["lieu" => $lieu->getId()]);

        $films = $filmsRepo->findAll();
        $filmsParId = [];
        if (!empty($films)) {
            foreach ($films as $film) {
                $filmsParId[$film->getId()] = $film;
            }
        }

        if (empty($lieu)) {
            return $this->abort(Response::HTTP_NOT_FOUND);
        } else {
            return $this->render($this->templateLieu, [
                "lieu" => $lieu,
                "seances" => $seances,
                "films" => $filmsParId,
            ]);
        }
    }
}
