<?php

namespace Bundle\Site\Controller;

class ProgrammationReseauController extends ProgrammationController
{
    protected $routePrefix = "reseau";
    protected $contenttypeFilms = "reso_films";
    protected $contenttypeSeances = "reso_seances";
    protected $contenttypeLieux = "reso_lieux";
    protected $contenttypeIntervenants = "reso_intervenants";
    protected $templateAccueil = "programmation/reseau.accueil.twig";
    protected $templateFilms = "programmation/reseau.films.twig";
    protected $templateIntervenants = "programmation/reseau.intervenants.twig";
    protected $templateSeances = "programmation/reseau.seances.twig";
    protected $templateArchives = "programmation/reseau.archives.twig";
    protected $templateLieux = "programmation/reseau.lieux.twig";
    protected $templateLieu = "programmation/reseau.lieu.twig";
}
