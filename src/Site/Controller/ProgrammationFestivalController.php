<?php

namespace Bundle\Site\Controller;

class ProgrammationFestivalController extends ProgrammationController
{
    protected $routePrefix = "festival";
    protected $contenttypeFilms = "fest_films";
    protected $contenttypeSeances = "fest_seances";
    protected $contenttypeLieux = "fest_lieux";
    protected $contenttypeIntervenants = "fest_intervenants";
    protected $templateAccueil = "programmation/festival.accueil.twig";
    protected $templateFilms = "programmation/festival.films.twig";
    protected $templateSeancesSpeciales = "programmation/festival.seancesspeciales.twig";
    protected $templateIntervenants = "programmation/festival.intervenants.twig";
    protected $templateSeances = "programmation/festival.grille.twig";
    protected $templateLieux = "programmation/festival.lieux.twig";
    protected $templateLieu = "programmation/festival.lieu.twig";
}
