<?php

declare(strict_types=1);

namespace Bundle\Site;

class CaptchaFailed extends \Exception
{
    public function __construct($details = "")
    {
        parent::__construct("La vérification reCAPTCHA a échoué. " . $details);
    }
}
